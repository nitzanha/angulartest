// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

//משתנה קבוע שהאפליקציה לא יכולה לשנות את הערכים שלו, משמש למטרה שהאפליקציה אמורה לעבוד בסביבות שונות
//בסביבת פיתוח נעבור מול השרת (סלים) בשונה מסביבת טסט שעובדת מקומית
//למשל כדי לדעת להבדיל בין הסביבות נשים אייקון של טסט, בעזרת המשתנה הנ"ל אנו נוכל להגדיר אם הוא יופיע או לא

//הגדרה מקומית localhost
export const environment = {
  production: false,
  url: 'http://localhost/angular/slim/',
  firebase:{
    apiKey: "AIzaSyAzo2EAzO7gs1geI8DFeuKoYvCc5lGWX90",
    authDomain: "angular-69d76.firebaseapp.com",
    databaseURL: "https://angular-69d76.firebaseio.com",
    projectId: "angular-69d76",
    storageBucket: "angular-69d76.appspot.com",
    messagingSenderId: "143106974021"
  }
};