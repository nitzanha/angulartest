import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { HttpParams, HttpHeaders } from '@angular/common/http';
import { AngularFireDatabase } from 'angularfire2/database';
import 'rxjs/Rx';


@Injectable()
export class UsersService {
  http:Http;

  getUsers(){
  //get users from the slim rest api (Don't say DB)
  return this.http.get(environment.url + 'users');
}

 getUser(id){
    return this.http.get(environment.url + 'users/' +id);
  }
  
  postUser(data){
    let options = {
      headers:new Headers({'content-type':'application/x-www-form-urlencoded'})
    }
    let params = new HttpParams().append('name',data.name).append('phoneNumber',data.phoneNumber);
    return this.http.post(environment.url + 'users',params.toString(),options);
  }

  deleteUser(key){ 
    return this.http.delete(environment.url + 'users/' +key);
  }

  getUsersFire(){
        return this.db.list('/users').valueChanges();
  }

  updateUser(id,user){
    let options = {
      headers: new Headers({'content-type': 'application/x-www-form-urlencoded'}
    )};
    var params = new HttpParams().append('name',user.name).append('phoneNumber',user.phoneNumber);
    return this.http.put(environment.url + 'users/'+id, params.toString(), options);      
  }

//----------------------------------------------------------------------------------------------------
getProducts(){
  
  return this.http.get(environment.url + 'products');
}

getProduct(id){
    return this.http.get(environment.url + 'products/' +id);
  }

   putProduct(data,id){
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    let params = new HttpParams().append('name',data.name).append('price',data.price);
    return this.http.put(environment.url + 'products/'+ id,params.toString(), options);
  }
  
  getProductsFire(){
        return this.db.list('/products').valueChanges();
  }
 
  login(credentials){ 
   let options = {
      headers:new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    let params = new HttpParams().append('user',credentials.user).append('password',credentials.password);
    return this.http.post(environment.url + 'auth',params.toString(),options).map(response=>{
      let token = response.json().token;
      if(token) localStorage.setItem('token',token);
      console.log(token);
    });
  }

   constructor(http:Http, private db:AngularFireDatabase) {
    this.http = http;
   }

}
