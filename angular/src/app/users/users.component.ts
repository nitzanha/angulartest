import { Component, OnInit } from '@angular/core';
import { UsersService } from './users.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from "@angular/router";



@Component({
  selector: 'users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  users;
  usersKeys;
  
  showSlim:Boolean = true;
  updates = [];
  lastOpenedToUpdate;
  fusers;

  addform = new FormGroup({
    name: new FormControl('',Validators.required),
    phoneNumber: new FormControl('',Validators.required)
  });


  updateform = new FormGroup({
    name: new FormControl('',Validators.required),
    phoneNumber: new FormControl('',Validators.required)
  });
  
  

   deleteUser(key){    
  let index = this.usersKeys.indexOf(key);
  this.usersKeys.splice(index,1);
  this.service.deleteUser(key).subscribe(
  response=> console.log(response));
  }

  updateUser(id){
    if(this.updateform.invalid) return;
    this.service.updateUser(id,this.updateform.value).subscribe(response =>{
      console.log(response);
      this.service.getUsers().subscribe(response => {
        this.users =  response.json();
        this.usersKeys = Object.keys(this.users);
      });      
    });

  }
  showUpdate(key){
    if(this.updates[key]){
      this.updates[key] = false;
    }else{
      if(this.lastOpenedToUpdate){
        this.updates[this.lastOpenedToUpdate] = false;
      }
      this.updates[key] = true;
      this.updateform.get('name').setValue(this.users[key].name);
      this.updateform.get('phoneNumber').setValue(this.users[key].phoneNumber);
      this.lastOpenedToUpdate = key;      
    } 
  }
 constructor(private service:UsersService,  private router:Router) {}

  //הצגת רשימת היוזרים
  ngOnInit() {
    this.service.getUsers().subscribe(response => {
      this.users =  response.json();
      this.usersKeys = Object.keys(this.users);
    });
  }
    

  
}
