import { Component, OnInit } from '@angular/core';
import { UsersService } from '../users.service';
import { Router } from "@angular/router";

@Component({
  selector: 'firebase',
  templateUrl: './firebase.component.html',
  styleUrls: ['./firebase.component.css']
})
export class FirebaseComponent implements OnInit {
users;
  constructor(private service:UsersService, private router:Router){ }

  ngOnInit() {
    this.service.getUsersFire().subscribe(response=>{
            console.log(response);
            this.users = response;
          })
        }
        

}