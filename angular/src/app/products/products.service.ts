import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { HttpParams, HttpHeaders } from '@angular/common/http';
import { AngularFireDatabase } from 'angularfire2/database';
import 'rxjs/Rx';

@Injectable()
export class UsersService {
  http:Http;

getProducts(){
  //get users from the slim rest api (Don't say DB)
  return this.http.get('http://localhost/angular/slim/products');
}
  constructor(http:Http, private db:AngularFireDatabase) {
    this.http = http;
   }

}
