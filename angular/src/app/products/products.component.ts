import { Component, OnInit } from '@angular/core';
//import { ProductsService } from './products.service';
import { UsersService } from './../users/users.service';



@Component({
  selector: 'products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
products;
  productsKeys;
  constructor(service:UsersService) {
    service.getProducts().subscribe(response=>{
        //console.log(response.json());
        this.products = response.json();
        this.productsKeys = Object.keys(this.products);
      });
 }
  ngOnInit() {
  }

}

