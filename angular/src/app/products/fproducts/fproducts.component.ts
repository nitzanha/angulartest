import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../users/users.service';
import { Router } from "@angular/router";


@Component({
  selector: 'fproducts',
  templateUrl: './fproducts.component.html',
  styleUrls: ['./fproducts.component.css']
})
export class FproductsComponent implements OnInit {

  constructor(private service:UsersService, private router:Router) { }
products;
  ngOnInit() {
  this.service.getProductsFire().subscribe(response=>{
            console.log(response);
            this.products = response;
          })
        }
}
