import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { UsersService } from '../../users/users.service';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {
  @Output() addUser: EventEmitter<any> = new EventEmitter<any>();
  @Output() addUserPs: EventEmitter<any> = new EventEmitter<any>();

  service:UsersService;
  prdform = new FormGroup({
  name:new FormControl(),
  price:new FormControl(),
  id:new FormControl()
    });

  constructor(private route: ActivatedRoute ,service: UsersService) {
    this.service = service;
   }
  //שליחת הנתונים
  sendData() {
    //הפליטה של העדכון לאב
  this.addUser.emit(this.prdform.value.message);
  console.log(this.prdform.value);
  this.route.paramMap.subscribe(params=>{
    let id = params.get('id');
    this.service.putProduct(this.prdform.value, id).subscribe(
      response => {
        console.log(response.json());
        this.addUserPs.emit();
      }
    );
  })
  }

  product;
  ngOnInit() {
    this.route.paramMap.subscribe(params=>{
      let id = params.get('id');
      console.log(id);
      this.service.getProduct(id).subscribe(response=>{
        this.product = response.json();
        console.log(this.product);
      })
  })

  }

}
