import { FormControl,FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { UsersService } from "../users/users.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
 invalid = false;
  loginform = new FormGroup({
	  user:new FormControl(),
	  password:new FormControl(),	    
  });

  login(){
  this.service.login(this.loginform.value).subscribe(response=>{
    this.router.navigate(['/']);
  },error =>{
    this.invalid = true;
  })
}

  logout(){ 
    localStorage.removeItem('auth');
    this.invalid= false; 
   }



   constructor(private service:UsersService, private router:Router) { }

  ngOnInit() {
  	var value = localStorage.getItem('auth');
  	
	if(value == 'true'){		
		this.router.navigate(['/users']);
	}else{
		this.router.navigate(['/login']);
	}
  }


}
