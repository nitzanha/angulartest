import { BrowserModule } from '@angular/platform-browser';
import { NgModule , Component } from '@angular/core';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { NavigationComponent } from './navigation/navigation.component';


import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { environment } from './../environments/environment';
import { UsersService } from './users/users.service';


import {AngularFireModule} from 'angularfire2';
import {AngularFireDatabaseModule} from 'angularfire2/database';
import { UsersFormComponent } from './users/users-form/users-form.component';
import { FirebaseComponent } from './users/firebase/firebase.component';
import { LoginComponent } from './login/login.component';
import { ProductsComponent } from './products/products.component';
import { SearchResultsComponent } from './products/search-results/search-results.component';
import { EditProductComponent } from './products/edit-product/edit-product.component';
import { FproductsComponent } from './products/fproducts/fproducts.component';
//import { ProductsService } from './products/products.service';



@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    NotFoundComponent,
    NavigationComponent,
    UsersFormComponent,
    FirebaseComponent,
    LoginComponent,
    ProductsComponent,
    SearchResultsComponent,
    EditProductComponent,
    FproductsComponent,

  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    RouterModule.forRoot([
      {path:'',component:ProductsComponent},
      {path: 'products', component: ProductsComponent},
      {path: 'search-results', component: SearchResultsComponent},
      {pathMatch: 'full',path: 'edit-product/:id', component: EditProductComponent},
      //{path:'users',component:UsersComponent},
      {path: 'fproducts', component: FproductsComponent},
      //{path: 'firebase', component: FirebaseComponent},
      {path:'login',component:LoginComponent},
      {path:'**',component:NotFoundComponent}

    ])
  ],
  providers: [UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }